# Uso de SonarQube

Es una herramienta de revisión automática de códigos para detectar bugs, vulnerabilities y code smells en su código. Se puede integrar con un flujo de trabajo existente para permitir la inspección continua de código en las ramas de su proyecto.

La plataforma desarrollada cubre por completo los 7 ejes de la calidad del código:
- Arquitectura y diseño.
- Comentarios.
- Reglas de codificación.
- Errores potenciales.
- Complejidad.
- Pruebas unitarias.
- Código duplicado

Para el uso de esta herramienta de analisis...

A continuacion se muestra el uso basico de la herramienta.

## SonarQube

Manual de usuario de SonarQube información y documentación mas específica pueden ver [acá][1].

### Arquitectura de alto nivel

La arquitectura que muestra Sonar es la representada en la Ilustración 4. En ella se
puede ver que se compone de tres componentes:
    - Base de Datos. Sonar necesita una base de datos donde almacenar todos los
resultados de cada análisis. Las bases de datos soportadas por Sonar son:
        -- Microsoft SQL Server 10.0 y 11.0.
        -- MySQL 5.1 y 5.5.
        -- Oracle 10G y 11G.
        -- PostgreSQL 8.x y 9.x.
-    Sonar-Runner. Esta es la aplicación proporcionada por Sonar para analizar el
código y guardar los resultados en la base de datos.
    - Interfaz Web. Es la interfaz web que proporciona Sonar para que los usuarios
vean el resultado del análisis llevado a cabo.

![Arquitectura SonarQube][2]

a que base de datos nos conectamos nosotros?


### Servidor Sonarqube con Docker

Precisamos tener un servidor SonarQube corriendo. Para esta tarea usaremos una imagen Docker para así agilizar este proceso. 

Entonces, podemos proceder a descargar el contenedor SonarQube haciendo un pull contra el repositorio de Docker:

![Sonarqube - Docker][3]

Una vez descargado debemos iniciarlo para dejarlo disponible en un determinado puerto. Para lograr esto ejecutamos:

> docker run -d --name sonarqube -p 9000:9000 -p 9092:9092 sonarqube

En esta línea le estamos especificando el nombre del contenedor que queremos correr y el mapeo de puertos entre el contenedor y nuestra red.


Para verificar la instalación podemos intentar acceder a http://ip-de-docker:puerto. En nuestro caso el puerto es 9000 y para saber la IP de nuestro contenedor podemos usar el comando “docker-machine ip default“.

Si todo salió bien, deberíamos ver algo como esta imagen:

![Docker - ip][4]

### Crear Proyecto en el servidor SonarQube

Primero se debe estar logueado en SonarQube y desde ahí nos dirigimos al menú de “Administration”. Luego, “Projects->Management” y ahí seleccionamos “Create Project”.

![create proyect][5]

En este formulario definimos el nombre que el proyecto va a tener en el servidor, la versión y la project key (que puede contener letras, números, ‘-‘, ‘_’, ‘.’ y ‘:’,y  al menos un dígito).

### Ejecutar SonarQube Scanner

Para poder ejecutar un análisis con SonarQube sobre un proyecto debemos descargar SonarQube Scanner aquí[Aqui][6]. Una vez descargado, se debe descomprimir y luego agregar al path la carpeta /bin que se encuentra dentro del directorio donde descomprimimos, para poder ejecutarlo desde línea de comandos fácilmente.

Para saber si la instalación fue exitosa, ejecutar:

>sonar-scanner -h

Se debería ver una salida como esta:

![scanner][7]

Una vez culminado el paso anterior, se debe configurar la ubicación del servidor SonarQube. Para esto hay que editar el archivo de propiedades <directorioSonarScanner>/conf/sonar-scanner.properties como se muestra debajo.

![Properties][8]

*(Quitar comentario para que tome la configuración)*

Para proceder con el análisis en cuestión, se necesita de la creación de un archivo de properties que es particular para cada proyecto, llamado sonar-project.properties (situado en la raíz del mismo):

![Sonar - Project][9]

En este indicamos los parámetros correspondientes al proyecto creado en el servidor SonarQube.

Habiendo configurado los 2 archivos, es momento de ejecutar el análisis. Para esto hay que situarse  en el directorio del proyecto y ejecutar el comando:

>sonar-scanner

Una vez finalizado el análisis, se desplegará en pantalla la URL para acceder al reporte del mismo.


## Fuentes

[1]: https://www.sonarqubehispano.org/display/DOC/Navegando+por+SonarQube
[2]: ./img/ArquitecturaSonarqube.png
[3]: ./img/sobarqube-docker-1.png
[4]: ./img/sobarqube-docker-2.png
[5]: ./img/create-project-sonarqube.png
[6]: https://docs.sonarqube.org/latest/analysis/scan/sonarscanner/
[7]: ./img/sonarqube-scanner.png
[8]: ./img/sonarqube-scanner-properties.png
[9]: ./img/sonar-project-properties.png




